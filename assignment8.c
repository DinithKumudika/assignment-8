#include<stdio.h>
#include<stdlib.h>


struct student_detail
{
    char student_fname[20];
    char subject[20];
    int *marks;
};

void getstudent(struct student_detail student[]);
void printstudent(struct student_detail student[]);

void getstudent(struct student_detail student[])
{
    for(int i=0; i<=5; i++)
    {
        printf("Student Name:");
        scanf("%s", student[i].student_fname);
        printf("Subject:");
        scanf("%s", student[i].subject);
        printf("Marks:");
        scanf("%d", &student[i].marks);
        printf("\n");
    }
}

void printstudent(struct student_detail student[])
{
    for(int i=0; i<=5; i++)
    {
        printf("Name of the student-: %s\n", student[i].student_fname);
        printf("Subject-: %s\n", student[i].subject);
        printf("Marks for %s-: %d\n", student[i].subject, student[i].marks);
        printf("\n");
    }
}

int main()
{
    struct student_detail student[10];
    printf("C programming Assignment 8\n");
    printf("\n");
    
    printf("<---STUDENT DETAIL FORM--->\n");
    printf("\n");
    
    
    getstudent(student);
    
    printf("\n");
    
    printf("<---RECORDED STUDENT DETAILS--->\n");
    printf("\n");
    
    printstudent(student);

    return 0;
}